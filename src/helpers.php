<?php

use Stevema\Afs\AfsManage;
use Stevema\Facades\Afs;
if(! function_exists('afs')){
    function afs(){
        if(function_exists('app')) {
            return app('afs');
        }
        $config_file_path = realpath(__DIR__.'/../Config/config.php');
        $config = require($config_file_path);
        return new AfsManage($config);
    }
}
if(! function_exists('afs_create')){
    function afs_create($scene, $sessionId){
        $afs = app('afs');
        return $afs->createToken($scene, $sessionId);
    }
}
if(! function_exists('afs_check')){
    function afs_check($scene, $sessionId, $token){
        $afs = app('afs');
        return $afs->checkToken($scene, $sessionId, $token);
    }
}
if(! function_exists('afs_error_message')){
    function afs_error_message(){
        $afs = app('afs');
        return $afs->getErrorMessage();
    }
}
