<?php
namespace Stevema\Facades;

use Stevema\Afs\AfsManage;
use Illuminate\Support\Facades\Facade;
class Afs extends Facade
{
    /**
     * Get the name of the class registered in the Application container.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'afs';
//        return AfsManage::class;
    }
}
