<?php

namespace Stevema\Afs;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Stevema\Facades\Afs;

class AfsProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton('afs', function(Application $app){
            return new AfsManage($app['config']->get('afs'));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // 发布配置文件
        $this->initPublishes();
        // 合并配置
        $this->mergeConfigFrom(
            realpath(__DIR__.'/Config/config.php'), 'afs'
        );
        // 自定义验证规则
        $this->initValidator();
    }
    protected function initPublishes(){
        // 发布配置文件
        if ($this->app->runningInConsole()) {
            $this->publishes([
                realpath(__DIR__.'/Config/config.php') => config_path('afs.php'),
//          $from => $to
//	        __DIR__.'.../xxx.php' => config_path('xxx.php'), //配置文件
//	        __DIR__.'.../migrations' => database_path('migrations'), //迁移文件
//	        __DIR__.'.../views' => resource_path('views/vendor'), //视图文件
//	        __DIR__.'.../translations' => resource_path('lang/vendor'), //翻译文件

            ],'afs');
        }
    }

    public function initValidator(){

        // 自定义验证规则
        Validator::extend('afs_scene', function ($attribute, $value, $parameters, $validator) {
            try {
                Afs::checkScene($value);
                return true;
            } catch (AfsException $e){
                return false;
            }catch (\Exception $e){
                return false;
            }
        });

        // 下面可以自定义验证消息，但是这个方法的文档我不太看得懂，自己写的。
        Validator::replacer('afs_scene', function ($message, $attribute, $rule, $parameters) {
//            dd($message, $attribute, $rule, $parameters);
            return str_replace($message,$attribute.' is not in whitelist',$message);
        });



        // 自定义验证规则
        Validator::extend('afs_token', function ($attribute, $value, $parameters, $validator) {
//            dd($attribute, $value, $parameters, $validator->getData());
            $data = $validator->getData();
            $scene_key = $parameters[0];
            $session_id_key = $parameters[1];
            return Afs::checkToken($data[$scene_key], $data[$session_id_key], $value);
        });

        // 下面可以自定义验证消息，但是这个方法的文档我不太看得懂，自己写的。
        Validator::replacer('afs_token', function ($message, $attribute, $rule, $parameters) {
//            dd($message, $attribute, $rule, $parameters);
            return str_replace($message,$attribute.' is validation failed',$message);
        });
    }
}
