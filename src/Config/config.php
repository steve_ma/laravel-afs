<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Afs Config
    |--------------------------------------------------------------------------
    |
    | 定义自己的Afs服务
    |
    */

    # 加密的字符串secret
    'afs_secret' => env("AFS_SECRET", "SteveMa"),
    # Afs 可以保留的时间 - 过了时间会提示超时 并且无法验证通过
    'afs_expire' => env("AFS_EXPIRE", 600),
    # 方式: nvc(无感知) slide(滑动) click(点击) 等 _ 场景
    # 比如 nvc_login_h5  无感知登录H5
    # 默认提供几个 这个是多个场景拼接的  这是字符串  设置env的时候请用逗号分隔
    'afs_scenes' => env("AFS_SCENES", implode(",", [
        'nvc_login','nvc_register','slide_login','slide_register',
        'click_login','click_register'
    ])),
];
