<?php
namespace Stevema\Afs;
//use Illuminate\Support\Facades\Crypt;
use Stevema\Afs\AfsCrypt as Crypt;

class AfsManage{
    /**
     * 配置信息
     * @var
     */
    protected $config;
    /**
     * 解密的错误信息
     * @var string
     */
    protected $error_message = 'ok';

    public function __construct(Array $config){
        $this->config = $config;
    }

    public function checkScene(String $scene):void
    {
        if(empty($scene)) throw new AfsException("scene is validation failed");
        # 场景值必须合法
        if(!in_array($scene, explode(",", $this->config['afs_scenes']))){
            throw new AfsException("scenes is not in whitelist");
        }
    }
    public function checkSessionId(String $session_id):void
    {
        if(empty($session_id)) throw new AfsException("session_id is validation failed");
    }

    public function createToken(String $scene, String $session_id){
        try{
            $this->checkScene($scene);
            $this->checkSessionId($session_id);
            $time = time()+$this->config['afs_expire'];
            # 然后开始加密
            return Crypt::encrypt([
                #第一个参数 scene
                $scene,
                # session_id
                $session_id,
                # secret
                $this->config['afs_secret'],
                # timestamp - 过期时间
                $time,
                #sign
                md5($scene.$session_id.$this->config['afs_secret'].$time),
            ], $this->config['afs_secret']);
        }catch (\Exception $e){
//            dd($e);
            throw new AfsException($e->getMessage());
        }
    }
    public function checkToken(String $scene, String $session_id, String $token){
        try{
            $this->checkScene($scene);
            $this->checkSessionId($session_id);
            # 然后开始解密
            try {
                [
                    #第一个参数 scene
                    $token_scene,
                    # session_id
                    $token_session_id,
                    # secret
                    $token_secret,
                    # timestamp - 过期时间
                    $token_timestamp,
                    #sign
                    $token_sign,
                ] = Crypt::decrypt($token, $this->config['afs_secret']);
            } catch (\Exception $e){
                throw new AfsException("token is validation failed 1");
            }
            # 解码后 验证一下
            if(md5($token_scene.$token_session_id.$token_secret.$token_timestamp) != $token_sign) {
                throw new AfsException("token is validation failed 2");
            }
            # scene session_id secret 是不是和初始化的一致
            if($token_scene != $scene || $token_session_id != $session_id || $token_secret != $this->config['afs_secret']){
                throw new AfsException("token is validation failed 3");
            }
            # 解码后 验证一下时间是不是超过了
            if(time() > intval($token_timestamp)) {
                throw new AfsException("token is already timed out");
            }
            return true;
        }catch (\Exception $e){
            $this->error_message = $e->getMessage();
            return false;
        }
    }
    public function getErrorMessage(){
        return $this->error_message;
    }
}
