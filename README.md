# laravel-afs

#### 介绍
自定义一个Afs服务-[不是阿里云的Afs调用]

### 安装教程

```shell

# 安装

$ composer require stevema/laravel-afs

```


### 使用说明

```php
# 1、生成配置文件
$ php artisan vendor:publish --tag="afs"

# 2、修改配置文件 /config/afs.php 或在 /.env 文件中添加配置
AFS_SECRET=  # 加密用到的串
AFS_EXPIRE=  # 过期时间 默认600(秒)
AFS_SCENES=  # 场景-多个中间加逗号分隔 默认: 'nvc_login,nvc_register,slide_login,slide_register,click_login,click_register'
```

### 目录说明

```
├─ src
│   ├─ Config           # 配置文件目录
│   │   └─ config.php   # 配置文件
│   ├─ Facades          # 门面文件目录
│   │   └─ Afs.php      # 门面
│   └─ AfsCrype.php     # 加密解密文件
│   └─ AfsException.php # 异常文件
│   └─ AfsManage.php    # 项目主文件
│   └─ AfsProvider.php  # 服务者
│   └─ Helper.php       # 帮助文件
└─ composer.json        # 配置

```

### 使用方式

```php
# 引用门面
use Stevema\Facades\Afs;

# 随便加一个路由
Route::get('/t/afs', function(){
    # 定义 scene 和 sessionId
    $scene = 'nvc_login';
    $sessionId = 'sessionId';
    
    # 第一种方式 来自门面 Stevema\Afs\Facades\Afs 上面use的
//    $token = Afs::createToken($scene,$sessionId);
//    $check = Afs::checkToken($scene,$sessionId, $token);
    
    # 如果不引入 这样用
//    $token = \Stevema\Facades\Afs::createToken($scene,$sessionId);
//    $check = \Stevema\Facades\Afs::checkToken($scene,$sessionId, $token);
    
    # 第二种方式 来自 app()
//    $afs = app('afs');
//    $token = $afs->createToken($scene,$sessionId);
//    $check = $afs->checkToken($scene,$sessionId, $token);
    
    # 第三种方式  来自 helper 
    $afs = afs();
    $token = $afs->createToken($scene,$sessionId);
    $check = $afs->checkToken($scene,$sessionId, $token);
    # 第四种方式 来自helper
    $token = afs_create($scene, $sessionId);
    $check = afs_check($scene, $sessionId, $token);
    $message = afs_error_message();
    dd([
        'scene' => $scene,
        'sessionId' => $sessionId,
        'token' => $token,
        'check' => $check,
        'message' => Afs::getErrorMessage()
    ]);
});

```

### controller 示例
```php

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AfsController extends Controller
{
    public function create(Request $request){
        $request->validate([
            'scene' => ['required','afs_scene',],//检测scene在这里  afs_scene 代替了new AfsCheckScene()
            'sessionId' => ['required',],
        ]);
        $data = $request->only("scene", 'sessionId');
//        $token = app('afs')->createToken($data['scene'],$data['sessionId']);
        $token = afs_create($data['scene'],$data['sessionId']);
        return response()->json(['token' => $token]);
    }
    public function check(Request $request){
        $request->validate([
            'scene' => ['required','afs_scene',],// afs_scene 代替了rule的实例对象
            'sessionId' => ['required',], //stevema
            'token' => ['required','afs_token:scene,sessionId'], //检测token在这里
            //afs_token 代替了new AfsCheckToken() :后面的是 scene 和sessionId    参数名已经可以自定义了
            //afs_token:场景参数名,sessionId参数名
        ]);
        return response()->json(['check' => true]);
    }
}

```


### 备注

还是有优化的地方-先这样吧
